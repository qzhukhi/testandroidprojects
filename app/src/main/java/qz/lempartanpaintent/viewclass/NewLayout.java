package qz.lempartanpaintent.viewclass;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import qz.lempartanpaintent.databinding.LayoutNewLayoutBinding;
import qz.lempartanpaintent.viewclass.data;

public class NewLayout extends AppCompatActivity {
    LayoutNewLayoutBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = LayoutNewLayoutBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        ArrayList mydata = data.mydata;
        //binding.result.append(String.valueOf(mydata.get(0)));
        data.mydata.forEach((n) -> binding.result.append(String.valueOf(n)));

    }
}
