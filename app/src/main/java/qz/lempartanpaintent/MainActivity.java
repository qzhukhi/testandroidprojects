package qz.lempartanpaintent;

import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.itsaky.androidide.logsender.LogSender;
import qz.lempartanpaintent.databinding.ActivityMainBinding;
import qz.lempartanpaintent.viewclass.NewLayout;
import qz.lempartanpaintent.viewclass.data;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Remove this line if you don't want AndroidIDE to show this app's logs
        LogSender.startLogging(this);
        super.onCreate(savedInstanceState);
        // Inflate and get instance of binding
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        // set content view to binding's root
        setContentView(binding.getRoot());
        binding.submit.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        String[] inputan = binding.input.getText().toString().split(",");

                        for (String index : inputan) {
                            data.mydata.add(inputan);
                        }

                        startActivity(new Intent(arg0.getContext(), NewLayout.class));
                    }
                });
    }
}
